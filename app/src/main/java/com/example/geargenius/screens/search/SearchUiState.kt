package com.example.geargenius.screens.search

data class SearchUiState(
    val searchQuery: String = "",
    val creaturesSelected: Boolean = true,
    val spellsSelected: Boolean = false,
)
