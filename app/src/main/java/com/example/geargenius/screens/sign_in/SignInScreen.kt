package com.example.geargenius.screens.sign_in

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.geargenius.common.composable.CustomAuthenticationButton
import com.example.geargenius.common.composable.CustomPasswordField
import com.example.geargenius.common.composable.CustomRedirectionLink
import com.example.geargenius.common.composable.CustomTextDivider
import com.example.geargenius.common.composable.CustomTextField
import com.example.geargenius.common.composable.CustomUnderlinedText
import com.example.geargenius.common.composable.HeadingText
import com.example.geargenius.common.composable.NormalText

@Composable
fun SignInScreen(
    openAndPopUp: (String,String) -> Unit,
    signInViewModel: SignInViewModel = hiltViewModel()
) {

    val uiState by signInViewModel.uiState

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .padding(28.dp),
        color = MaterialTheme.colorScheme.background
    ) {
        Column(
            modifier = Modifier.fillMaxSize()
        ) {
            NormalText(value = "Hey there, ")
            HeadingText(value = "Welcome back.")
            Spacer(modifier = Modifier.height(20.dp))
            CustomTextField(
                value = uiState.email,
                labelText = "Email",
                icon = Icons.Default.Email,
                onValueChanged = {
                    signInViewModel.onEmailChanged(it)
                },
                keyboardOptions = KeyboardOptions.Default
            )
            CustomPasswordField(
                value = uiState.password,
                onValueChanged = {
                    signInViewModel.onPasswordChanged(it)
                },
            )
            //CustomSignupCheckbox()
            Spacer(modifier = Modifier.height(20.dp))
            CustomUnderlinedText(value = "Forgot your password?")
            Spacer(modifier = Modifier.height(40.dp))
            CustomAuthenticationButton(
                value = "Sign in",
                onClick = {
                    signInViewModel.onSignInClicked(openAndPopUp)
                }
            )
            Spacer(modifier = Modifier.height(20.dp))
            CustomTextDivider()
            CustomRedirectionLink(
                onLoginScreen = true,
                onClick = {
                    signInViewModel.onRedirectClicked(openAndPopUp)
                }
            )
        }
    }
}

@Preview
@Composable
fun PreviewOfSignupScreen() {
    //SignInScreen()
}