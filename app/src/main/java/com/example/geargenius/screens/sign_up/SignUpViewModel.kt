package com.example.geargenius.screens.sign_up

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.geargenius.common.extension.isValidEmail
import com.example.geargenius.common.extension.isValidPassword
import com.example.geargenius.model.service.AccountService
import com.example.geargenius.navigation.AuthenticationScreen
import com.example.geargenius.utility.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val accountService: AccountService
) : ViewModel() {

    var uiState = mutableStateOf(SignUpUiState())

    private val email
        get() = uiState.value.email

    private val password
        get() = uiState.value.password

    private val isPrivacyPolicyRead
        get() = uiState.value.isPrivacyPolicyRead

    fun onEmailChanged(newValue: String) {
        uiState.value = uiState.value.copy(email = newValue)
    }

    fun onPasswordChanged(newValue: String) {
        uiState.value = uiState.value.copy(password = newValue)
    }

    fun onCheckBoxChecked(newValue: Boolean) {
        uiState.value = uiState.value.copy(isPrivacyPolicyRead = newValue)
    }

    fun onSignUpClicked(
        openAndPopUp: (String, String) -> Unit
    ) {

        if(!email.isValidEmail()) {
            return
        }

        if(!password.isValidPassword()) {
            return
        }

        if(!isPrivacyPolicyRead){
            return
        }

        viewModelScope.launch {
            Log.d("uiState", "SIGNED UP.")
            if(accountService.signUp(email, password) is Resource.Success)
                openAndPopUp(AuthenticationScreen.SignInScreen.route, AuthenticationScreen.SignUpScreen.route)
        }
    }

    fun onRedirectClicked(
        openAndPopUp: (String, String) -> Unit
    ) {
        Log.d("uiState", "REDIRECTED.")
        openAndPopUp(AuthenticationScreen.SignInScreen.route, AuthenticationScreen.SignUpScreen.route)
    }

}