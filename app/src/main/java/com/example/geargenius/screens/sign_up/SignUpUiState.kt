package com.example.geargenius.screens.sign_up

data class SignUpUiState(
    val email: String = "",
    val password: String = "",
    val isPrivacyPolicyRead: Boolean = false
)
