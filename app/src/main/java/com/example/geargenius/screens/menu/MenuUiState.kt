package com.example.geargenius.screens.menu

data class MenuUiState (
    val newUserEmailTextField: String = "",
    val newUserPasswordTextField: String = "",
    val currentUserEmailTextField: String = "",
    val currentUserPasswordTextField: String = "",
)
