package com.example.geargenius.screens.wiki

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.example.geargenius.common.composable.HeadingText

@Composable
fun InformationScreen() {
    Surface(
        modifier = Modifier.fillMaxSize()
    ) {
        HeadingText(value = "INFORMATION")
    }
}