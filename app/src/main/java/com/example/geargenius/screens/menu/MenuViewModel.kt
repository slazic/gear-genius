package com.example.geargenius.screens.menu

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.geargenius.model.service.AccountService
import com.example.geargenius.navigation.BottomNavigationScreen
import com.example.geargenius.navigation.Graph
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

sealed class DialogType {
    object ChangeEmail : DialogType(
    )
    object ChangePassword : DialogType(
    )
    object DeleteAccount : DialogType(
    )
}

@HiltViewModel
class MenuViewModel @Inject constructor(
    private val accountService: AccountService
) : ViewModel() {

    val showDialog = mutableStateOf(false)

    val uiState = mutableStateOf(MenuUiState())

    val selectedDialogType = mutableStateOf<DialogType?>(null)

    val userEmail = mutableStateOf(accountService.currentUser?.email)

    val isChanging = mutableStateOf(false)

    private fun isChangingChangeState(newValue: Boolean) {
        isChanging.value = newValue
    }

    fun onSignOutClicked(
        openAndPopUp: (String, String) -> Unit
    ) {
        Log.d("uiState", "SIGNED OUT.")
        accountService.signOut()
        openAndPopUp(Graph.ROOT, BottomNavigationScreen.MenuScreen.route)
    }

    fun onNewEmailSetClick(currentEmail: String, password: String, newEmail: String) {
        viewModelScope.launch {
            try {
                isChangingChangeState(true)
                accountService.changeEmail(currentEmail, password, newEmail)
                userEmail.value = newEmail
            } catch (e: Exception) {
                Log.d("e", e.toString())
            } finally {
                isChangingChangeState(false)
            }
        }
    }

    fun onNewPasswordSetClick(currentEmail: String, currentPassword: String, newPassword: String) {
        viewModelScope.launch {
            try {
                isChangingChangeState(true)
                accountService.changePassword(currentEmail, currentPassword, newPassword)
            } catch (e: Exception) {
                Log.d("e", e.toString())
            } finally {
                isChangingChangeState(false)
            }
        }
    }

    fun onDeleteAccountClick(currentEmail: String, currentPassword: String) {
        viewModelScope.launch {
            try {
                isChangingChangeState(true)
                accountService.deleteAccount(currentEmail, currentPassword)
            } catch (e: Exception) {
                Log.d("", e.toString())
            } finally {
                isChangingChangeState(false)
            }
        }
    }

    fun onShowDialogChange(newValue: Boolean) {
        showDialog.value = newValue
    }

    fun onDialogTypeSelected(dialogType: DialogType) {
        selectedDialogType.value = dialogType
    }

    fun onDialogNewEmailTextFieldChange(newValue: String) {
        uiState.value = uiState.value.copy(newUserEmailTextField = newValue)
    }

    fun onDialogNewPasswordTextFieldChange(newValue: String) {
        uiState.value = uiState.value.copy(newUserPasswordTextField = newValue)
    }

    fun onDialogCurrentEmailTextFieldChange(newValue: String) {
        uiState.value = uiState.value.copy(currentUserEmailTextField = newValue)
    }

    fun onDialogCurrentPasswordTextFieldChange(newValue: String) {
        uiState.value = uiState.value.copy(currentUserPasswordTextField = newValue)
    }
}