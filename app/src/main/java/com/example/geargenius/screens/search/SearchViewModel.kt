package com.example.geargenius.screens.search

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.geargenius.model.SearchResult
import com.example.geargenius.model.service.DataService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val dataService: DataService
) : ViewModel() {

    val uiState = mutableStateOf(SearchUiState())

    private val searchQuery
        get() = uiState.value.searchQuery

    private val creaturesSelected
        get() = uiState.value.creaturesSelected

    private val spellsSelected
        get() = uiState.value.spellsSelected

    private val _searchResult = MutableStateFlow<SearchResult<*>?>(null)
    //val searchResult: StateFlow<SearchResult<*>?> get() = _searchResult

    val searchResult = _searchResult

    val isSearching = mutableStateOf(false)

    private fun isSearchingChangeState(newValue: Boolean) {
        isSearching.value = newValue
    }

    fun onSearchQueryChanged(newValue: String) {
            uiState.value = uiState.value.copy(searchQuery= newValue)
    }

    fun onCreaturesSelected() {
        uiState.value = uiState.value.copy(creaturesSelected = true)
        uiState.value = uiState.value.copy(spellsSelected = false)
        Log.d("uiState", "CREATURES: $creaturesSelected")
        Log.d("uiState", "SPELLS: $spellsSelected")
    }

    fun onSpellsSelected() {
        uiState.value = uiState.value.copy(creaturesSelected = false)
        uiState.value = uiState.value.copy(spellsSelected = true)
        Log.d("uiState", "SPELLS: $spellsSelected")
        Log.d("uiState", "CREATURES: $creaturesSelected")
    }

    fun onSearchClicked(searchQuery: String) {
        viewModelScope.launch {
            Log.d("uiState", "DATA FETCH")
            isSearchingChangeState(true)
            if(creaturesSelected) {

                try {
                    /*val result = dataService.searchCreature(creatureName= searchQuery).first()
                    Log.d("uiState", result.data.toString())
                    Log.d("uiState", result.message.toString())
                    Log.d("uiState", result.exception.toString())
                    _searchResult.value = result.data */
                    dataService.searchCreature(creatureName = searchQuery).collect {
                        _searchResult.value = it.data
                    }
                } catch (e: Exception) {
                    // catch error TODO
                } finally {
                    isSearchingChangeState(false)
                }
            } else if(spellsSelected) {

                try {
                    /*val result = dataService.searchSpell(spellName = searchQuery).first()
                    Log.d("uiState", result.data.toString())
                    Log.d("uiState", result.message.toString())
                    Log.d("uiState", result.exception.toString())
                    _searchResult.value = result.data */
                    dataService.searchSpell(spellName = searchQuery).collect {
                        _searchResult.value = it.data
                    }
                } catch (e: Exception) {
                    // catch error TODO
                } finally {
                    isSearchingChangeState(false)
                }
            }
        }
    }
}