package com.example.geargenius.screens.search

import android.util.Log
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.common.composable.ListContent
import com.example.geargenius.common.composable.LoadingIndicator
import com.example.geargenius.common.composable.ResultMenu
import com.example.geargenius.common.composable.SearchBox
import com.example.geargenius.common.extension.isScrolled

@Composable
fun SearchScreen(
    searchViewModel: SearchViewModel = hiltViewModel(),
    openUp: (String) -> Unit,
    appViewModel: GearGeniusAppViewModel
) {

    val lazyListState = rememberLazyListState()
    //val searchResult by remember { mutableStateOf(searchViewModel.searchResult.value) }
    val searchResult = searchViewModel.searchResult.collectAsState()
    //val searchResult = rememberUpdatedState(newValue = searchViewModel.searchResult)
    val isSearching = searchViewModel.isSearching.value

    Log.d("uiState",  searchResult.value?.results.toString())
    Surface(
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            SearchBox(
                modifier = Modifier
                    .fillMaxWidth()
                    .animateContentSize(animationSpec = tween(durationMillis = 500))
                    .height(height = if (lazyListState.isScrolled) 0.dp else 120.dp),
                searchViewModel= searchViewModel
            )
            ResultMenu(
                initialText = if(searchResult.value?.results?.isNotEmpty() == true) "Result" else "No result found"
            )
            if (isSearching) {
                LoadingIndicator()
            } else {
                searchResult.value?.let {
                    ListContent(
                        state = lazyListState,
                        openUp = openUp,
                        cardInformation = it.results,
                        appViewModel= appViewModel
                    )
                }
            }
        }
    }
}