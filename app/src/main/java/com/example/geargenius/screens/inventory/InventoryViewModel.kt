package com.example.geargenius.screens.inventory

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InventoryViewModel @Inject constructor(

) : ViewModel() {

    val showDialog = mutableStateOf(false)

    val selectedBoxType = mutableStateOf<BoxType?>(null)

    fun onShowDialogChange(newValue: Boolean) {
        showDialog.value = newValue
    }

    fun onBoxTypeSelected(boxType: BoxType) {
        selectedBoxType.value = boxType
    }

}