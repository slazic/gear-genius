package com.example.geargenius.screens.inventory

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.HotelClass
import androidx.compose.material.icons.filled.RemoveModerator
import androidx.compose.material.icons.filled.WavingHand
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.input.KeyboardType
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.common.composable.ArmorPeirceBox
import com.example.geargenius.common.composable.CriticalMultiplierBox
import com.example.geargenius.common.composable.DamageBox
import com.example.geargenius.common.composable.InventoryDialog

sealed class BoxType(
        val title: String,
        val icon: ImageVector,
        val label: String,
) {
        object DamagePercentage : BoxType(
                title = "Damage ( % )",
                icon = Icons.Default.WavingHand,
                label = "Percentage"
        )

        object DamageFlat : BoxType(
                title = "Damage ( + )",
                icon = Icons.Default.WavingHand,
                label = "Flat"
        )
        object ArmorPeirce : BoxType(
                title = "Armor Peirce ( % )",
                icon = Icons.Default.RemoveModerator,
                label = "Percentage"
        )
        object CriticalMultiplier : BoxType(
                title = "Critical Multiplier",
                icon = Icons.Default.HotelClass,
                label = "Multiplier"
        )
}


@Composable
fun InventoryScreen(
        inventoryViewModel: InventoryViewModel = hiltViewModel(),
        appViewModel: GearGeniusAppViewModel
) {
        val showDialog = inventoryViewModel.showDialog.value
        val inventoryUiState = appViewModel.uiState.value
        val selectedBoxType = inventoryViewModel.selectedBoxType.value

        Surface(
                modifier = Modifier.fillMaxSize()
        ) {
                Column(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalAlignment = Alignment.CenterHorizontally
                ) {

                        if (showDialog) {
                                Log.d("inventory" , "HERE")
                                Log.d("inventory", selectedBoxType.toString())
                                selectedBoxType?.let { boxType ->
                                        when (boxType) {
                                                is BoxType.DamagePercentage -> InventoryDialog(
                                                        onDismiss = {
                                                                inventoryViewModel.onShowDialogChange(it)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        title = BoxType.DamagePercentage.title,
                                                        textFieldValue = inventoryUiState.dialogTextField,
                                                        textFieldIcon = BoxType.DamagePercentage.icon,
                                                        textFieldLabel = BoxType.DamagePercentage.label,
                                                        onValueChanged = {
                                                                         appViewModel.onDialogTextFieldChange(it)
                                                        },
                                                        onSetClicked = {
                                                                appViewModel.onInventoryDamagePercentageSet(it.toInt())
                                                                inventoryViewModel.onShowDialogChange(false)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        keyboardOptions = KeyboardOptions.Default.copy(
                                                                keyboardType = KeyboardType.Number
                                                        ),
                                                )
                                                is BoxType.DamageFlat -> InventoryDialog(
                                                        onDismiss = {
                                                                inventoryViewModel.onShowDialogChange(it)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        title = BoxType.DamageFlat.title,
                                                        textFieldValue = inventoryUiState.dialogTextField,
                                                        textFieldIcon = BoxType.DamageFlat.icon,
                                                        textFieldLabel = BoxType.DamageFlat.label,
                                                        onValueChanged = {
                                                                appViewModel.onDialogTextFieldChange(it)
                                                        },
                                                        onSetClicked = {
                                                                appViewModel.onInventoryDamageFlatSet(it.toInt())
                                                                inventoryViewModel.onShowDialogChange(false)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        keyboardOptions = KeyboardOptions.Default.copy(
                                                                keyboardType = KeyboardType.Number
                                                        ),
                                                )
                                                is BoxType.ArmorPeirce -> InventoryDialog(
                                                        onDismiss = {
                                                                inventoryViewModel.onShowDialogChange(it)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        title = BoxType.ArmorPeirce.title,
                                                        textFieldValue = inventoryUiState.dialogTextField,
                                                        textFieldIcon = BoxType.ArmorPeirce.icon,
                                                        textFieldLabel = BoxType.ArmorPeirce.label,
                                                        onValueChanged = {
                                                                appViewModel.onDialogTextFieldChange(it)
                                                        },
                                                        onSetClicked = {
                                                                appViewModel.onInventoryArmorPiercingPercentageSet(it.toInt())
                                                                inventoryViewModel.onShowDialogChange(false)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        keyboardOptions = KeyboardOptions.Default.copy(
                                                                keyboardType = KeyboardType.Number
                                                        ),
                                                )
                                                is BoxType.CriticalMultiplier -> InventoryDialog(
                                                        onDismiss = {
                                                                inventoryViewModel.onShowDialogChange(it)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        title = BoxType.CriticalMultiplier.title,
                                                        textFieldValue = inventoryUiState.dialogTextField,
                                                        textFieldIcon = BoxType.CriticalMultiplier.icon,
                                                        textFieldLabel = BoxType.CriticalMultiplier.label,
                                                        onValueChanged = {
                                                                appViewModel.onDialogTextFieldChange(it)
                                                        },
                                                        onSetClicked = {
                                                                appViewModel.onCriticalMultiplierSet(it.toDouble())
                                                                inventoryViewModel.onShowDialogChange(false)
                                                                appViewModel.onDialogTextFieldChange("")
                                                        },
                                                        keyboardOptions = KeyboardOptions.Default.copy(
                                                                keyboardType = KeyboardType.Number
                                                        ),
                                                )

                                                else -> {}
                                        }
                                }

                        }

                        DamageBox(
                                onClickPercentage = {
                                        inventoryViewModel.onShowDialogChange(showDialog.not())
                                        inventoryViewModel.onBoxTypeSelected(BoxType.DamagePercentage)
                                        Log.d("inventory", showDialog.toString())
                                },
                                onClickFlat = {
                                        inventoryViewModel.onShowDialogChange(showDialog.not())
                                        inventoryViewModel.onBoxTypeSelected(BoxType.DamageFlat)
                                },
                                damagePercentage= inventoryUiState.inventoryDamagePercentage.toString(),
                                damageFlat= inventoryUiState.inventoryDamageFlat.toString()
                        )
                        ArmorPeirceBox(
                                onClickPercentage = {
                                        inventoryViewModel.onShowDialogChange(showDialog.not())
                                        inventoryViewModel.onBoxTypeSelected(BoxType.ArmorPeirce)
                                },
                                armorPeircePercentage= inventoryUiState.inventoryArmorPiercingPercentage.toString()
                        )
                        CriticalMultiplierBox(
                                onClickMultiplier = {
                                        inventoryViewModel.onShowDialogChange(showDialog.not())
                                        inventoryViewModel.onBoxTypeSelected(BoxType.CriticalMultiplier)
                                },
                                criticalMultiplier = inventoryUiState.criticalMultiplier.toString()
                        )
                }
        }
}