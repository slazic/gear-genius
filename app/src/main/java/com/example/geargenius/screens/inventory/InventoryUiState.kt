package com.example.geargenius.screens.inventory

data class InventoryUiState(
    val inventoryDamagePercentage : Int = 0,
    val inventoryDamageFlat: Int = 0,
    val inventoryArmorPiercingPercentage: Int = 0,
    val criticalMultiplier: Double = 1.0,
    val dialogTextField: String = "",
)