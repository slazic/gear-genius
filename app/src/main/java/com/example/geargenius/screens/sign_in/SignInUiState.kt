package com.example.geargenius.screens.sign_in

data class SignInUiState(
    val email: String = "",
    val password: String = "",
)
