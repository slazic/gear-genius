package com.example.geargenius.screens.sign_in

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.geargenius.common.extension.isValidEmail
import com.example.geargenius.common.extension.isValidPassword
import com.example.geargenius.model.service.AccountService
import com.example.geargenius.navigation.AuthenticationScreen
import com.example.geargenius.navigation.Graph
import com.example.geargenius.utility.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val accountService: AccountService
) : ViewModel() {

    var uiState = mutableStateOf(SignInUiState())

    private val email
        get() = uiState.value.email

    private val password
        get() = uiState.value.password

    fun onEmailChanged(newValue: String) {
        uiState.value = uiState.value.copy(email = newValue)
    }

    fun onPasswordChanged(newValue: String) {
        uiState.value = uiState.value.copy(password = newValue)
    }

    fun onSignInClicked(
        openAndPopUp: (String, String) -> Unit
    ) {

        if(!email.isValidEmail()) {
            return
        }

        if(!password.isValidPassword()) {
            return
        }

       viewModelScope.launch {
           Log.d("uiState", "SIGNED IN.")
           if(accountService.signIn(email, password) is Resource.Success)
               openAndPopUp(Graph.HOME, AuthenticationScreen.SignInScreen.route)
       }
    }

    fun onRedirectClicked(
        openAndPopUp: (String, String) -> Unit
    ) {
        Log.d("uiState", "REDIRECTED.")
        openAndPopUp(AuthenticationScreen.SignUpScreen.route, AuthenticationScreen.SignInScreen.route)
    }

}