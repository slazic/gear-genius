package com.example.geargenius.screens.splash

class SplashUiState(
    var x: Float = 0f,
    var y: Float = 0f,
    var z: Float = 0f,
    var tX: Float = 0f,
    var tY: Float = 0f,
)