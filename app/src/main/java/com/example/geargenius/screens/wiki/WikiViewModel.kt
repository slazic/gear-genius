package com.example.geargenius.screens.wiki

import androidx.lifecycle.ViewModel
import com.example.geargenius.model.service.AccountService
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WikiViewModel @Inject constructor(
    private val accountService: AccountService
) : ViewModel() {

}