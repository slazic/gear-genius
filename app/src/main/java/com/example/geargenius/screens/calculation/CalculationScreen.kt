package com.example.geargenius.screens.calculation

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.common.composable.CalculationBox
import com.example.geargenius.common.composable.ResultBox
import com.example.geargenius.common.extension.isScrolled
import com.example.geargenius.utility.StringRegex

@Composable
fun CalculationScreen(
        appViewModel: GearGeniusAppViewModel,
        openUp: (String) -> Unit,
) {
        val inventoryState = appViewModel.uiState.value
        val gearDamagePercentage = 1.0 + (inventoryState.inventoryDamagePercentage.toDouble() / 100)
        val gearDamageFlat = inventoryState.inventoryDamageFlat.toDouble()
        val gearArmorPeirce = 1.0 + (inventoryState.inventoryArmorPiercingPercentage.toDouble() / 100)

        val lazyListState = rememberLazyListState()
        val selectedSpells = appViewModel.selectedSpells
        val selectedCreature = appViewModel.selectedCreature.value

        val selectedWardsAndBlades = mutableListOf<Double>()
        val selectedDamageSpell = mutableMapOf<String, Int?>()

        val incBoostMap = StringRegex.extractPercentageAndElement(selectedCreature.inc_boost.toString())
        val incResistMap = StringRegex.extractPercentageAndElement(selectedCreature.inc_resist.toString())

        selectedSpells.forEach {
                if (it.description?.startsWith("Applies") == true)
                        selectedWardsAndBlades.add(StringRegex.extractAppliesPercentage(it.description))
                else if (it.description?.startsWith("Deals") == true){
                        selectedDamageSpell.clear()
                        selectedDamageSpell.putAll(StringRegex.extractDealsNumberWithSchool(it.description))
                }
        }

        val damageSpell = selectedDamageSpell.entries.firstOrNull()
        var baseDamage = damageSpell?.value?.toDouble()

        var absorb = 0.0
        /*TODO NAPRAVI FLOOR NAKON SVAKOG IZRACUNA ZA BLADE, WARD, AURA...*/
        if (baseDamage != null) {

                baseDamage *= gearDamagePercentage //gear damage percentage
                baseDamage += gearDamageFlat // + gear damage flat

                selectedWardsAndBlades.forEach {selectedWardOrBlade -> //na base damage se dodaju wards i traps
                        if(selectedWardOrBlade % 100 == 0.0) {
                                absorb = selectedWardOrBlade
                        } else if (selectedWardOrBlade < 1.0 ){
                                baseDamage = baseDamage!! * selectedWardOrBlade * gearArmorPeirce//if ward is shield then pierce it
                        } else {
                                baseDamage = baseDamage!! * selectedWardOrBlade
                        }
            }

                baseDamage = baseDamage!! - absorb //absorb se oduzima na kraju

                damageSpell?.key?.let { spellKey ->
                        if (incBoostMap.containsKey(spellKey)) {
                                baseDamage = baseDamage!! * (1.0 + (incBoostMap[spellKey]?.toDouble() ?: 0.0) / 100.0)
                        } else if (incResistMap.containsKey(spellKey)) {
                                baseDamage = baseDamage!! * (1.0 - (incResistMap[spellKey]?.toDouble() ?: 0.0) / 100.0)
                        }
                }

                baseDamage = baseDamage!! * inventoryState.criticalMultiplier

                if (baseDamage!! < 0) baseDamage = 0.0

        }

        val remainingHealth = if (selectedCreature.health != null && baseDamage != null)
                selectedCreature.health.minus(baseDamage!!.toInt())
        else selectedCreature.health ?: 0


        Surface(
                modifier = Modifier.fillMaxSize(),
        ) {
                Column {
                        ResultBox(
                                height = if (lazyListState.isScrolled) 0.dp else Dp.Unspecified,
                                selectedCreature= selectedCreature,
                                incBoost= incBoostMap,
                                incResist= incResistMap,
                                damageDealt= baseDamage?.toInt(),
                                remainingHealth= remainingHealth
                        )
                        LazyColumn(
                                state= lazyListState,
                                content = {
                                item {
                                        CalculationBox(
                                                openUp= openUp,
                                                appViewModel= appViewModel,
                                                title = "Selected Spells",
                                                selectedSpells= selectedSpells,
                                                damageDealt= baseDamage?.toInt()
                                        )
                                }
                        })
                }

        }
}

/*@Preview
@Composable
fun PreviewCalculationScreen() {
        CalculationScreen(appViewModel = hiltViewModel())
}*/