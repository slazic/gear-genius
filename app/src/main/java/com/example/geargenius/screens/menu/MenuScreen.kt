package com.example.geargenius.screens.menu

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.geargenius.common.composable.LoadingIndicator
import com.example.geargenius.common.composable.MainMenu
import com.example.geargenius.common.composable.MenuDialog

@Composable
fun MenuScreen(
    openAndPopUp: (String,String) -> Unit,
    menuViewModel: MenuViewModel = hiltViewModel(),
) {

    val showDialog = menuViewModel.showDialog.value
    val menuUiState = menuViewModel.uiState.value
    val selectedDialogType = menuViewModel.selectedDialogType.value

    val isChanging = menuViewModel.isChanging.value


    Surface(
        modifier = Modifier
            .fillMaxSize()
            //.padding(top = 10.dp)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            if(showDialog) {
                selectedDialogType?.let { dialogType ->
                    when(dialogType) {
                        is DialogType.ChangeEmail -> MenuDialog(
                            dialogType = selectedDialogType,
                            onDismiss = {
                                menuViewModel.onShowDialogChange(it)
                                menuViewModel.onDialogNewEmailTextFieldChange("")
                            },
                            title = "Change Email",
                            currentEmailTextFieldValue = menuUiState.currentUserEmailTextField,
                            currentUserPasswordTextFieldValue = menuUiState.currentUserPasswordTextField,
                            newTextFieldValue = menuUiState.newUserEmailTextField,
                            onCurrentEmailValueChanged = {
                                menuViewModel.onDialogCurrentEmailTextFieldChange(it)
                            },
                            onNewValueChanged = {
                                menuViewModel.onDialogNewEmailTextFieldChange(it)
                            },
                            onUserPasswordValueChanged = {
                                menuViewModel.onDialogCurrentPasswordTextFieldChange(it)
                            },
                            onSetClicked = {
                                menuViewModel.onNewEmailSetClick(
                                    menuUiState.currentUserEmailTextField,
                                    menuUiState.currentUserPasswordTextField,
                                    menuUiState.newUserEmailTextField
                                )
                                menuViewModel.onShowDialogChange(false)
                                menuViewModel.onDialogCurrentEmailTextFieldChange("")
                                menuViewModel.onDialogCurrentPasswordTextFieldChange("")
                                menuViewModel.onDialogNewEmailTextFieldChange("")
                            },
                            keyboardOptions = KeyboardOptions.Default,
                        )

                        is DialogType.ChangePassword -> MenuDialog(
                            dialogType= selectedDialogType,
                            onDismiss = {
                                menuViewModel.onShowDialogChange(it)
                                menuViewModel.onDialogNewEmailTextFieldChange("")
                            },
                            title = "Change Password",
                            currentEmailTextFieldValue = menuUiState.currentUserEmailTextField,
                            currentUserPasswordTextFieldValue = menuUiState.currentUserPasswordTextField,
                            newTextFieldValue = menuUiState.newUserPasswordTextField,
                            onCurrentEmailValueChanged = {
                                menuViewModel.onDialogCurrentEmailTextFieldChange(it)
                            },
                            onNewValueChanged = {
                                menuViewModel.onDialogNewPasswordTextFieldChange(it)
                            },
                            onUserPasswordValueChanged = {
                                menuViewModel.onDialogCurrentPasswordTextFieldChange(it)
                            },
                            onSetClicked = {
                                menuViewModel.onNewPasswordSetClick(
                                    menuUiState.currentUserEmailTextField,
                                    menuUiState.currentUserPasswordTextField,
                                    menuUiState.newUserPasswordTextField
                                )
                                menuViewModel.onShowDialogChange(false)
                                menuViewModel.onDialogCurrentEmailTextFieldChange("")
                                menuViewModel.onDialogCurrentPasswordTextFieldChange("")
                                menuViewModel.onDialogNewPasswordTextFieldChange("")
                            },
                            keyboardOptions = KeyboardOptions.Default,
                        )

                        is DialogType.DeleteAccount -> MenuDialog(
                            dialogType= selectedDialogType,
                            onDismiss = {
                                menuViewModel.onShowDialogChange(it)
                                menuViewModel.onDialogNewEmailTextFieldChange("")
                            },
                            title = "Delete Account",
                            currentEmailTextFieldValue = menuUiState.currentUserEmailTextField,
                            currentUserPasswordTextFieldValue = menuUiState.currentUserPasswordTextField,
                            newTextFieldValue = "",
                            onCurrentEmailValueChanged = {
                                menuViewModel.onDialogCurrentEmailTextFieldChange(it)
                            },
                            onNewValueChanged = { },
                            onUserPasswordValueChanged = {
                                menuViewModel.onDialogCurrentPasswordTextFieldChange(it)
                            },
                            onSetClicked = {
                                menuViewModel.onDeleteAccountClick(
                                    menuUiState.currentUserEmailTextField,
                                    menuUiState.currentUserPasswordTextField,
                                )
                                menuViewModel.onShowDialogChange(false)
                                menuViewModel.onDialogCurrentEmailTextFieldChange("")
                                menuViewModel.onDialogCurrentPasswordTextFieldChange("")
                            },
                            keyboardOptions = KeyboardOptions.Default,
                        )

                        else -> {}
                    }
                }
            }

            MainMenu(
                userEmail= menuViewModel.userEmail.value ?: "",
                onSignOutClick = {
                    menuViewModel.onSignOutClicked(openAndPopUp)
                },
                onChangeEmailClick = {
                    menuViewModel.onShowDialogChange(showDialog.not())
                    menuViewModel.onDialogTypeSelected(DialogType.ChangeEmail)
                },
                onChangePasswordClick = {
                    menuViewModel.onShowDialogChange(showDialog.not())
                    menuViewModel.onDialogTypeSelected(DialogType.ChangePassword)
                },
                onAccountDeleteClick = {
                    menuViewModel.onShowDialogChange(showDialog.not())
                    menuViewModel.onDialogTypeSelected(DialogType.DeleteAccount)
                }
            )
            
            Spacer(modifier = Modifier.padding(top=10.dp))
            
            if (isChanging) {
                LoadingIndicator()
            }
        }
    }
}