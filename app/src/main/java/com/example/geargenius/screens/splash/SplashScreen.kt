package com.example.geargenius.screens.splash

import android.util.Log
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.geargenius.R
import com.example.geargenius.navigation.AuthenticationScreen
import com.example.geargenius.navigation.Graph
import com.example.geargenius.navigation.RootScreen

@Composable
fun SplashScreen(
    openAndPopUp: (String,String) -> Unit,
    isUserSignedOut: Boolean,
    splashScreenUiState: SplashUiState
) {
    var startAnimation by remember { mutableStateOf(false) }

    val alphaAnimation = animateFloatAsState(
        targetValue = if (startAnimation) 1f else 0f,
        animationSpec = tween(
            durationMillis = 4000
        )
    )

    LaunchedEffect(key1 = true) {
        startAnimation = true
    }

    Log.d("sensorRoot", "xSplashScreen: " + splashScreenUiState.x.toString())
    Log.d("sensorRoot", "ySplashScreen: " + splashScreenUiState.y.toString())

    Surface(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colorScheme.background)
            .clickable {
                when {
                    isUserSignedOut -> openAndPopUp(
                        AuthenticationScreen.SignInScreen.route,
                        RootScreen.SplashScreen.route
                    )

                    else -> openAndPopUp(Graph.HOME, RootScreen.SplashScreen.route)
                }
            },
    ) {
        Logo(alpha=alphaAnimation.value, splashScreenUiState= splashScreenUiState)
    }
}

@Composable
fun Logo(
    splashScreenUiState: SplashUiState,
    alpha: Float
) {

    Log.d("sensorRoot", "xLogo: " + splashScreenUiState.x.toString())
    Log.d("sensorRoot", "yLogo: " + splashScreenUiState.y.toString())


    Box(
        modifier= Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.launcher_icon),
                contentDescription = null,
                modifier = Modifier
                    .height(300.dp)
                    .width(300.dp)
                    .alpha(alpha = alpha)
                    .graphicsLayer {
                        this.rotationX = splashScreenUiState.x
                        this.rotationY = splashScreenUiState.y
                        this.rotationZ = splashScreenUiState.z
                        this.translationX = splashScreenUiState.tX
                        this.translationY = splashScreenUiState.tY
                    }
            )
            Spacer(modifier = Modifier.height(100.dp))
            Text(
                text = "GearGenius",
                fontSize = 48.sp,
                fontFamily = FontFamily.Monospace,
                color = Color.White,
                modifier = Modifier.alpha(alpha= alpha),
                fontWeight = FontWeight.ExtraBold,
                fontStyle = FontStyle.Italic
            )
            Text(
                text = "Powered by FERIT",
                fontSize = 24.sp,
                fontFamily = FontFamily.Monospace,
                color = Color.White,
                modifier = Modifier.alpha(alpha= alpha),
            )
        }
    }
}