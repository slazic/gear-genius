package com.example.geargenius.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.common.composable.BottomNavigationBar
import com.example.geargenius.common.composable.TopApplicationBar
import com.example.geargenius.navigation.HomeNavigationGraph

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    appViewModel: GearGeniusAppViewModel,
    rootNavController: NavHostController
) {

    val navController: NavHostController = rememberNavController()

    Scaffold(
        topBar = {
            TopApplicationBar(navController= navController)
        },
        bottomBar = {
            BottomNavigationBar(navController= navController)
        },
        content = {innerPadding ->
            HomeNavigationGraph(
                appViewModel= appViewModel,
                navController= navController,
                rootNavController= rootNavController,
                modifier = Modifier.padding(top = innerPadding.calculateTopPadding(), bottom = innerPadding.calculateBottomPadding(),
                )
            )
        }
    )
}