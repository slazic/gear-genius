package com.example.geargenius.screens.wiki

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.geargenius.common.composable.WikiCard
import com.example.geargenius.common.composable.WikiHomeCardInformation

@Composable
fun WikiScreen(
    openUp: (String) -> Unit
) {

    val information = listOf(
        WikiHomeCardInformation.Loremaster,
        WikiHomeCardInformation.Jabberwock,
        WikiHomeCardInformation.Zeus,
        WikiHomeCardInformation.LordNightshade,
        WikiHomeCardInformation.TheHarvestLord
    )

    Surface {
        Column() {
            LazyColumn(
            ) {
                item {
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = 10.dp, top=5.dp),
                        text = "Popular",
                        style = TextStyle(
                            fontSize = 30.sp,
                            fontWeight = FontWeight.Bold,
                            fontStyle = FontStyle.Normal
                        ),
                    )
                }
                items(information.size) { index ->
                    WikiCard(information = information[index] ,openUp= openUp)
                }
            }
        }

    }
}