package com.example.geargenius

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.geargenius.navigation.RootNavigationGraph
import com.example.geargenius.screens.splash.SplashUiState
import com.example.geargenius.ui.theme.GearGeniusTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity(), SensorEventListener {

    private val splashScreenUiState = mutableStateOf(SplashUiState())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GearGeniusTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val rootNavController: NavHostController = rememberNavController()

                    val appViewModel: GearGeniusAppViewModel = hiltViewModel()

                    val isUserSignedOut = appViewModel.getAuthState().collectAsState().value

                    val sensorManager: SensorManager = getSystemService(SENSOR_SERVICE) as SensorManager

                    sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)?.also {
                        sensorManager.registerListener(
                            this,
                            it,
                            SensorManager.SENSOR_DELAY_FASTEST,
                            SensorManager.SENSOR_DELAY_FASTEST,
                            )
                    }

                    RootNavigationGraph(isUserSignedOut= isUserSignedOut, rootNavController= rootNavController, appViewModel= appViewModel, splashScreenUiState= splashScreenUiState.value)
                }
            }
        }
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_ACCELEROMETER){
            val axisX = event.values[0]
            val axisY = event.values[1]
            val axisZ = event.values[2]

            val x = axisY * 3f
            val y = axisX * 3f
            val z = -axisX
            val tX = axisX * -10
            val tY = axisY * 10

            splashScreenUiState.value = SplashUiState(x, y, z, tX, tY)
            Log.d("sensorRoot", "xMain: " + splashScreenUiState.value.x.toString())
            Log.d("sensorRoot", "yMain: " + splashScreenUiState.value.y.toString())

        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        return
    }
}