package com.example.geargenius.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.common.extension.navigateAndPopUp
import com.example.geargenius.common.extension.navigateWithSingleTop
import com.example.geargenius.screens.calculation.CalculationScreen
import com.example.geargenius.screens.inventory.InventoryScreen
import com.example.geargenius.screens.menu.MenuScreen
import com.example.geargenius.screens.search.SearchScreen
import com.example.geargenius.screens.wiki.InformationScreen
import com.example.geargenius.screens.wiki.WikiScreen

@Composable
fun HomeNavigationGraph(
    appViewModel: GearGeniusAppViewModel,
    navController: NavHostController,
    rootNavController: NavHostController,
    modifier: Modifier
) {
    NavHost(
        modifier = modifier,
        navController = navController,
        route = Graph.HOME,
        startDestination = BottomNavigationScreen.WikiScreen.route
    ) {
        composable(route = BottomNavigationScreen.WikiScreen.route) {
            WikiScreen(
                openUp= { route ->
                    navController.navigateWithSingleTop(route)
                },
            )
        }
        composable(route = BottomNavigationScreen.CalculationScreen.route) {
            CalculationScreen(
                appViewModel= appViewModel,
                openUp = { route ->
                    navController.navigateWithSingleTop(route)
                }
            )
        }
        composable(route = BottomNavigationScreen.ProfileScreen.route) {
            InventoryScreen(
                appViewModel= appViewModel
            )
        }
        composable(route = BottomNavigationScreen.MenuScreen.route) {
            MenuScreen(
                openAndPopUp= { route, popUp ->
                    rootNavController.navigateAndPopUp(route, popUp)
                },
            )
        }

        detailsNavGraph(
            navController = navController,
            appViewModel= appViewModel
        )
    }
}

fun NavGraphBuilder.detailsNavGraph(
    navController: NavHostController,
    appViewModel: GearGeniusAppViewModel
) {
    navigation(
        route = Graph.DETAILS,
        startDestination = DetailsScreen.InformationScreen.route
    ) {
        composable(route = DetailsScreen.InformationScreen.route) {
            InformationScreen()
        }
        composable(route = DetailsScreen.SearchScreen.route) {
            SearchScreen(
                openUp= { route ->
                    navController.navigateWithSingleTop(route)
                },
                appViewModel= appViewModel
            )
        }
        /*composable(route = DetailsScreen.Overview.route) {
            ScreenContent(name = DetailsScreen.Overview.route) {
                navController.popBackStack(
                    route = DetailsScreen.Information.route,
                    inclusive = false
                )
            }
        } */
    }
}