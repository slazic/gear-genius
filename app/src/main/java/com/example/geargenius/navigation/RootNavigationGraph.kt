package com.example.geargenius.navigation

import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.common.extension.navigateAndPopUp
import com.example.geargenius.screens.HomeScreen
import com.example.geargenius.screens.sign_in.SignInScreen
import com.example.geargenius.screens.sign_up.SignUpScreen
import com.example.geargenius.screens.splash.SplashScreen
import com.example.geargenius.screens.splash.SplashUiState

@Composable
fun RootNavigationGraph(
    appViewModel: GearGeniusAppViewModel,
    isUserSignedOut: Boolean,
    rootNavController: NavHostController,
    splashScreenUiState: SplashUiState
) {

    Log.d("sensorRoot", "xRoot: " + splashScreenUiState.x.toString())
    Log.d("sensorRoot", "yRoot: " + splashScreenUiState.y.toString())


    NavHost(
        navController = rootNavController,
        route = Graph.ROOT,
        startDestination = RootScreen.SplashScreen.route

    ) {

        composable(route = RootScreen.SplashScreen.route) {
            SplashScreen(
                openAndPopUp= { route, popUp ->
                    rootNavController.navigateAndPopUp(route, popUp)
                },
                isUserSignedOut= isUserSignedOut,
                splashScreenUiState= splashScreenUiState
            )
        }

        composable(route = AuthenticationScreen.SignInScreen.route) {
            SignInScreen(
                openAndPopUp= { route, popUp ->
                    rootNavController.navigateAndPopUp(route, popUp)
                }
            )
        }
        composable(route = AuthenticationScreen.SignUpScreen.route) {
            SignUpScreen(
                openAndPopUp= { route, popUp ->
                    rootNavController.navigateAndPopUp(route, popUp)
                }
            )
        }

        composable(route = Graph.HOME) {
            HomeScreen(rootNavController = rootNavController, appViewModel= appViewModel)
        }

    }
}