package com.example.geargenius.navigation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Dashboard
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.MenuBook
import androidx.compose.material.icons.filled.Newspaper
import androidx.compose.ui.graphics.vector.ImageVector

object Graph {
    const val ROOT = "root"
    const val AUTHENTICATION = "auth"
    const val HOME = "home"
    const val DETAILS = "details"
}

sealed class RootScreen(val route: String) {
    object SplashScreen: RootScreen(route = "splash_screen")
}

sealed class DetailsScreen(val route: String) {
    object InformationScreen: DetailsScreen(route = "information_screen")
    object OverviewScreen: DetailsScreen(route = "overview_screen")

    object SearchScreen: DetailsScreen(route = "search_screen")
}

sealed class AuthenticationScreen(val route: String) {
    object SignInScreen: AuthenticationScreen(route = "sign_in_screen")
    object SignUpScreen: AuthenticationScreen(route = "sign_up_screen")
    object ForgotPasswordScreen: AuthenticationScreen(route = "forgot_password_screen")
}

sealed class BottomNavigationScreen(
    val route: String,
    val title: String,
    val imageVector: ImageVector
) {
    object WikiScreen: BottomNavigationScreen(
        route = "wiki_screen",
        title = "Wiki",
        imageVector = Icons.Default.Newspaper
    )
    object CalculationScreen: BottomNavigationScreen(
        route = "calculation_screen",
        title = "Spell Book",
        imageVector = Icons.Default.MenuBook
    )
    object ProfileScreen: BottomNavigationScreen(
        route = "profile_screen",
        title = "Inventory",
        imageVector = Icons.Default.Dashboard
    )
    object MenuScreen: BottomNavigationScreen(
        route = "menu_screen",
        title = "Menu",
        imageVector = Icons.Default.Menu
    )
}