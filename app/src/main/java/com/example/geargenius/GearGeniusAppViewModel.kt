package com.example.geargenius

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.geargenius.model.Creature
import com.example.geargenius.model.Spell
import com.example.geargenius.model.service.AccountService
import com.example.geargenius.screens.inventory.InventoryUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class GearGeniusAppViewModel @Inject constructor(
    private val accountService: AccountService,
) : ViewModel() {

    val selectedCreature = mutableStateOf(Creature())

    private val _selectedSpells = mutableStateListOf<Spell>()
    val selectedSpells: List<Spell> = _selectedSpells

    var uiState = mutableStateOf(InventoryUiState())

    fun getAuthState() = accountService.getAuthState(viewModelScope)

    val userEmail get() = accountService.currentUser?.email
    val isEmailVerified get() = accountService.currentUser?.isEmailVerified ?: false

    fun onSpellAdd(spell: Spell) {
        if (spell.type == "Damage" || spell.type == "Steal") {
            val existingSpell = _selectedSpells.find { it.type == "Damage" || it.type == "Steal" }
            if (existingSpell != null) {
                val index = _selectedSpells.indexOf(existingSpell)
                _selectedSpells[index] = spell
            } else {
                _selectedSpells.add(spell)
            }
        } else {
            _selectedSpells.add(spell)
        }
    }

    fun onSpellRemove(index: Int) {
        _selectedSpells.removeAt(index)
    }

    fun onClearAllSpells() {
        _selectedSpells.clear()
        selectedCreature.value = Creature()
    }

    fun onCreatureAdd(creature: Creature) {
        selectedCreature.value = creature
    }

    fun onInventoryDamagePercentageSet(newValue: Int) {
        uiState.value = uiState.value.copy(inventoryDamagePercentage = newValue)
    }

    fun onInventoryDamageFlatSet(newValue: Int) {
        uiState.value = uiState.value.copy(inventoryDamageFlat = newValue)
    }

    fun onInventoryArmorPiercingPercentageSet(newValue: Int) {
        uiState.value = uiState.value.copy(inventoryArmorPiercingPercentage = newValue)
    }

    fun onCriticalMultiplierSet(newValue: Double) {
        uiState.value = uiState.value.copy(criticalMultiplier = newValue)
    }

    fun onDialogTextFieldChange(newValue: String) {
        uiState.value = uiState.value.copy(dialogTextField = newValue)
    }


}