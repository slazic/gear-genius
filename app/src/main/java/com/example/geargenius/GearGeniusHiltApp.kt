package com.example.geargenius

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GearGeniusHiltApp: Application() {
}