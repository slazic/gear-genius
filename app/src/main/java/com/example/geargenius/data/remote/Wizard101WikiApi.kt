package com.example.geargenius.data.remote

import com.example.geargenius.model.Creature
import com.example.geargenius.model.SearchResult
import com.example.geargenius.model.Spell
import retrofit2.http.GET
import retrofit2.http.Path

interface Wizard101WikiApi {

    @GET("/creatures/{creatureName}")
    suspend fun searchCreature(
        @Path("creatureName") creatureName: String
    ) : SearchResult<Creature>

    @GET("/spells/{spellName}")
    suspend fun searchSpell(
        @Path("spellName") spellName: String
    ) : SearchResult<Spell>

}