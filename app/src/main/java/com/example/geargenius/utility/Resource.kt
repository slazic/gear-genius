package com.example.geargenius.utility

sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null,
    val exception: Throwable? = null,
) {
    class Success<T>(data: T) : Resource<T>(data = data)
    class Error<T>(errorMessage: String) : Resource<T>(message = errorMessage)
    class Loading<T> : Resource<T>()
    class Exception<T>(exception: Throwable): Resource<T>(exception= exception)
}
