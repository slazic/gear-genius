package com.example.geargenius.utility

object StringRegex {

    fun extractPercentageAndElement(input: String): Map<String, Int> {
        val pattern = Regex("""(\d+)%\s+([\w,\s]+)""")
        val matchResults = pattern.findAll(input)
        val result = mutableMapOf<String, Int>()

        for (matchResult in matchResults) {
            val number = matchResult.groupValues[1].toIntOrNull() ?: 0
            val elements = matchResult.groupValues[2].split(", ").map { it.trim() }
            for (element in elements) {
                result[element] = number
            }
        }

        return result
    }

    fun extractDealsNumberWithSchool(input: String): Map<String, Int?> {
        val pattern = Regex("""Deals\s+(\d+)(?:-(\d+))?\s+(Fire|Ice|Life|Storm|Death|Myth|Balance)""")
        val matchResult = pattern.find(input)

        return matchResult?.let { result ->
            val firstNumber = result.groupValues[1].toInt()
            val secondNumber = result.groupValues[2].toIntOrNull()
            val school = result.groupValues[3]

            val extractedValue = if (secondNumber != null)
                (firstNumber + secondNumber) / 2
            else
                firstNumber

            mapOf(school to extractedValue)
        } ?: emptyMap()
    }

    fun extractAppliesPercentage(input: String): Double {
        val percentagePattern = Regex("""([+-]?\d+)%""")
        val pureNumberPattern = Regex("""([+-]?\d+)""")
        val percentageMatchResult = percentagePattern.find(input)
        val pureNumberMatchResult = pureNumberPattern.find(input)

        return when {
            percentageMatchResult != null -> {
                val percentage = percentageMatchResult.groupValues[1].toDouble()

                if (input.contains("+") || input.contains("-")) {
                    if (percentage < 0) {
                        1 - (-percentage) / 100
                    } else {
                        1 + percentage / 100
                    }
                } else {
                    percentage / 100
                }
            }
            pureNumberMatchResult != null -> {
                pureNumberMatchResult.groupValues[1].toDouble()
            }
            else -> 0.0
        }
    }












}