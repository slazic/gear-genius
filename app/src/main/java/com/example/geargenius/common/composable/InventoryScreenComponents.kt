package com.example.geargenius.common.composable

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.HotelClass
import androidx.compose.material.icons.filled.RemoveModerator
import androidx.compose.material.icons.filled.WavingHand
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun DamageBox(
    onClickPercentage: () -> Unit,
    onClickFlat: () -> Unit,
    damagePercentage: String,
    damageFlat: String,
) {
    Surface(
        modifier = Modifier
            .padding(bottom = 5.dp, top = 20.dp)
            .fillMaxWidth(),
        tonalElevation = 3.dp
    ) {
        Column {
            Row(
                modifier = Modifier
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    }
                    .padding(20.dp),

                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(0.2f)
                ) {
                    Icon(
                        modifier = Modifier
                            .width(40.dp)
                            .height(40.dp),
                        imageVector = Icons.Default.WavingHand,
                        contentDescription = ""
                    )
                }
                Column(
                    modifier = Modifier.weight(0.8f)
                ) {
                    Text(
                        text = "DAMAGE ( % / + )",
                        fontSize = MaterialTheme.typography.titleLarge.fontSize
                    )
                }
            }
            Row(
                modifier = Modifier
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    }
                    .padding(start = 20.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(0.4f),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(text = "Percentage")
                }
                Column(
                    modifier = Modifier.weight(0.6f),
                    horizontalAlignment = Alignment.End
                ) {
                    IconButton(
                        modifier = Modifier.width(70.dp),
                        onClick = { onClickPercentage() },
                        colors = IconButtonDefaults.iconButtonColors(
                            contentColor = MaterialTheme.colorScheme.primary
                        )
                    ) {
                        Text(text = "+$damagePercentage%")
                    }
                }
            }
            Row(
                modifier = Modifier
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    }
                    .padding(start = 20.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(0.4f),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(text = "Flat")
                }
                Column(
                    modifier = Modifier.weight(0.6f),
                    horizontalAlignment = Alignment.End
                ) {
                    IconButton(
                        modifier = Modifier.width(70.dp),
                        onClick = { onClickFlat() },
                        colors = IconButtonDefaults.iconButtonColors(
                            contentColor = MaterialTheme.colorScheme.primary
                        )
                    ) {
                        Text(text = "+$damageFlat")
                    }
                }
            }
        }
    }
}

@Composable
fun ArmorPeirceBox(
    onClickPercentage: () -> Unit,
    armorPeircePercentage: String,
) {
    Surface(
        modifier = Modifier
            .padding(bottom = 5.dp, top = 20.dp)
            .fillMaxWidth(),
        tonalElevation = 3.dp
    ) {
        Column {
            Row(
                modifier = Modifier
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    }
                    .padding(20.dp),

                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(0.2f)
                ) {
                    Icon(
                        modifier = Modifier
                            .width(40.dp)
                            .height(40.dp),
                        imageVector = Icons.Default.RemoveModerator,
                        contentDescription = ""
                    )
                }
                Column(
                    modifier = Modifier.weight(0.8f)
                ) {
                    Text(
                        text = "ARMOR PIERCING",
                        fontSize = MaterialTheme.typography.titleLarge.fontSize
                    )
                }
            }
            Row(
                modifier = Modifier
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    }
                    .padding(start = 20.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(0.4f),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(text = "Percentage")
                }
                Column(
                    modifier = Modifier.weight(0.6f),
                    horizontalAlignment = Alignment.End
                ) {
                    IconButton(
                        modifier = Modifier.width(70.dp),
                        onClick = { onClickPercentage() },
                        colors = IconButtonDefaults.iconButtonColors(
                            contentColor = MaterialTheme.colorScheme.primary
                        )
                    ) {
                        Text(text = "+$armorPeircePercentage%")
                    }
                }
            }
        }
    }
}

@Composable
fun CriticalMultiplierBox(
    onClickMultiplier: () -> Unit,
    criticalMultiplier: String,
) {
    Surface(
        modifier = Modifier
            .padding(bottom = 5.dp, top = 20.dp)
            .fillMaxWidth(),
        tonalElevation = 3.dp
    ) {
        Column {
            Row(
                modifier = Modifier
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    }
                    .padding(20.dp),

                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(0.2f)
                ) {
                    Icon(
                        modifier = Modifier
                            .width(40.dp)
                            .height(40.dp),
                        imageVector = Icons.Default.HotelClass,
                        contentDescription = ""
                    )
                }
                Column(
                    modifier = Modifier.weight(0.8f)
                ) {
                    Text(
                        text = "CRITICAL MULTIPLIER",
                        fontSize = MaterialTheme.typography.titleLarge.fontSize
                    )
                }
            }
            Row(
                modifier = Modifier
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    }
                    .padding(start = 20.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(0.4f),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(text = "Multiplier")
                }
                Column(
                    modifier = Modifier.weight(0.6f),
                    horizontalAlignment = Alignment.End
                ) {
                    IconButton(
                        modifier = Modifier.width(70.dp),
                        onClick = { onClickMultiplier() },
                        colors = IconButtonDefaults.iconButtonColors(
                            contentColor = MaterialTheme.colorScheme.primary
                        )
                    ) {
                        Text(text = criticalMultiplier + "X")
                    }
                }
            }
        }
    }
}

@Composable
fun InventoryDialog(
    onDismiss: (Boolean) -> Unit,
    title: String,
    textFieldValue: String,
    textFieldLabel: String,
    textFieldIcon: ImageVector,
    onValueChanged: (String) -> Unit,
    onSetClicked: (String) -> Unit,
    keyboardOptions: KeyboardOptions
) {
    AlertDialog(
        onDismissRequest = { onDismiss(false) },
        title = {
            Text(
                text = title,
            )
        },
        text = {
            Column(
                modifier = Modifier.fillMaxWidth()
            ) {
                CustomTextField(
                    value = textFieldValue,
                    labelText = textFieldLabel,
                    icon = textFieldIcon,
                    onValueChanged = {
                        onValueChanged(it)
                    },
                    keyboardOptions = keyboardOptions
                )
            }
        },
        confirmButton = {
            TextButton(
                onClick = {
                    onSetClicked(textFieldValue)
                }
            ) {
                Text("Set")
            }
        },
        dismissButton = {
            TextButton(onClick = { onDismiss(false) }) {
                Text("Cancel")
            }
        },
        modifier = Modifier.padding(5.dp),
        shape = RectangleShape,
    )
}

@Preview
@Composable
fun PreviewInventoryBox() {
    Column {
        //DamageBox()
        //ArmorPeirceBox()
        //CriticalMultiplierBox()
        //InventoryDialog(onDismiss = {})
    }
}