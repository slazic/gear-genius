package com.example.geargenius.common.composable

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.geargenius.common.extension.navigateAndPopUp
import com.example.geargenius.common.extension.navigateWithSingleTop
import com.example.geargenius.common.extension.popUp
import com.example.geargenius.navigation.BottomNavigationScreen
import com.example.geargenius.navigation.DetailsScreen

val screens = listOf(
    BottomNavigationScreen.WikiScreen,
    BottomNavigationScreen.CalculationScreen,
    BottomNavigationScreen.ProfileScreen,
    BottomNavigationScreen.MenuScreen
)

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopApplicationBar(
    navController:NavHostController
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    val title = when (currentDestination?.route) {
        BottomNavigationScreen.WikiScreen.route -> BottomNavigationScreen.WikiScreen.title
        BottomNavigationScreen.CalculationScreen.route -> BottomNavigationScreen.CalculationScreen.title
        BottomNavigationScreen.ProfileScreen.route -> BottomNavigationScreen.ProfileScreen.title
        BottomNavigationScreen.MenuScreen.route -> BottomNavigationScreen.MenuScreen.title
        else -> ""
    }

    val inBottomNavigationDestination = screens.any { it.route == currentDestination?.route }

    Surface(
        tonalElevation = 3.dp
    ) {
        TopAppBar(
            title = {
                Text(text = title)
            },
            navigationIcon = {
                if (!inBottomNavigationDestination) {
                    IconButton(onClick = {
                        navController.popUp()
                    }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = "Back"
                        )
                    }
                }
            },

            actions = {
                if(currentDestination?.route == BottomNavigationScreen.WikiScreen.route) {
                    IconButton(onClick = {
                        navController.navigateWithSingleTop(DetailsScreen.SearchScreen.route)
                    }) {
                        Icon(
                            imageVector = Icons.Filled.Search,
                            contentDescription = "Search"
                        )
                    }
                }
            },
            colors = TopAppBarDefaults.smallTopAppBarColors(
            )
        )
    }

}

@Composable
fun BottomNavigationBar(
    navController: NavHostController
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    val bottomBarDestination = screens.any { it.route == currentDestination?.route }
    if(bottomBarDestination) {
        NavigationBar {
            screens.forEach { screen ->
                NavigationBarItem(
                    label = {
                            Text(text = screen.title)
                    },
                    alwaysShowLabel = true,
                    icon = {
                           Icon(
                               imageVector = screen.imageVector,
                               contentDescription = ""
                           )
                    },
                    selected = currentDestination?.hierarchy?.any {
                        it.route == screen.route
                    } == true,
                    colors = NavigationBarItemDefaults.colors(
                        unselectedIconColor = LocalContentColor.current.copy(alpha = 0.38f),
                    ),
                    onClick = {
                        currentDestination?.route?.let {
                            navController.navigateAndPopUp(screen.route, it)
                        }
                    }
                )
            }
        }
    }
}