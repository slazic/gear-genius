package com.example.geargenius.common.composable

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp
import com.example.geargenius.screens.menu.DialogType


@Composable
fun MenuDialog(
    dialogType: DialogType,
    onDismiss: (Boolean) -> Unit,
    title: String,
    currentEmailTextFieldValue: String,
    currentUserPasswordTextFieldValue: String,
    newTextFieldValue: String,
    onCurrentEmailValueChanged: (String) -> Unit,
    onNewValueChanged: (String) -> Unit,
    onUserPasswordValueChanged: (String) -> Unit,
    onSetClicked: () -> Unit,
    keyboardOptions: KeyboardOptions
) {
    AlertDialog(
        onDismissRequest = { onDismiss(false) },
        title = {
            Text(
                text = title,
            )
        },
        text = {
            Column(
                modifier = Modifier.fillMaxWidth()
            ) {
                CustomTextField(
                    value = currentEmailTextFieldValue,
                    labelText = "Current Email",
                    icon = Icons.Default.Email,
                    onValueChanged = {
                        onCurrentEmailValueChanged(it)
                    },
                    keyboardOptions = KeyboardOptions.Default
                )
                CustomPasswordField(
                    value = currentUserPasswordTextFieldValue,
                    onValueChanged = {
                        onUserPasswordValueChanged(it)
                    },
                )
                Spacer(modifier = Modifier.height(30.dp))
                when(dialogType) {
                    is DialogType.ChangeEmail -> CustomTextField(
                        value = newTextFieldValue,
                        labelText = "New Email",
                        icon = Icons.Default.Email,
                        onValueChanged = {
                            onNewValueChanged(it)
                        },
                        keyboardOptions = keyboardOptions
                    )

                    is DialogType.ChangePassword -> CustomPasswordField(
                        value = newTextFieldValue,
                        onValueChanged = {
                            onNewValueChanged(it)
                        },
                    )

                    else -> {}
                }
            }
        },
        confirmButton = {
            TextButton(
                onClick = {
                    onSetClicked()
                }
            ) {
                Text("Set")
            }
        },
        dismissButton = {
            TextButton(onClick = { onDismiss(false) }) {
                Text("Cancel")
            }
        },
        modifier = Modifier.padding(5.dp),
        shape = RectangleShape,
    )
}

@Composable
fun MainMenu(
    userEmail: String,
    onSignOutClick : () -> Unit,
    onChangeEmailClick: () -> Unit,
    onChangePasswordClick: ()->Unit,
    onAccountDeleteClick: () -> Unit,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth(),
    ) {
        MenuSection(value = "MORE INFORMATION")
        Surface(
            tonalElevation = 3.dp
        ) {
            Column {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .drawBehind {
                            drawLine(
                                color = Color.Gray,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = (1).dp.toPx()
                            )
                        }
                        .padding(end = 5.dp),
                    horizontalArrangement = Arrangement.End
                ) {
                    TextButton(
                        onClick = { /*TODO*/ },
                        //modifier= Modifier.width(100.dp)
                    ) {
                        Text(
                            text = "Privacy Policy",
                            fontSize = MaterialTheme.typography.bodyLarge.fontSize,
                        )
                    }
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .drawBehind {
                            drawLine(
                                color = Color.Gray,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = (1).dp.toPx()
                            )
                        }
                        .padding(end = 5.dp),
                    horizontalArrangement = Arrangement.End
                ) {
                    TextButton(
                        onClick = { /*TODO*/ },
                        ///modifier= Modifier.width(125.dp)
                    ) {
                        Text(
                            text = "Terms Of Service",
                            fontSize = MaterialTheme.typography.bodyLarge.fontSize,
                        )
                    }
                }
            }
        }
        MenuSection(value = "PERSONAL DATA")
        Surface(
            tonalElevation = 3.dp
        ) {
            Column {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .drawBehind {
                            drawLine(
                                color = Color.Gray,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = (1).dp.toPx()
                            )
                        }
                        .padding(end = 5.dp),
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        modifier= Modifier.padding(start= 10.dp),
                        text = "Email: ",
                        fontSize = MaterialTheme.typography.bodyLarge.fontSize,
                    )
                    TextButton(
                        onClick = { onChangeEmailClick() },
                        //modifier= Modifier.width(60.dp)
                    ) {
                        Text(
                            text = userEmail,
                            fontSize = MaterialTheme.typography.bodyLarge.fontSize,
                        )
                    }
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .drawBehind {
                            drawLine(
                                color = Color.Gray,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = (1).dp.toPx()
                            )
                        }
                        .padding(end = 5.dp),
                    horizontalArrangement = Arrangement.End
                ) {
                    TextButton(
                        onClick = { onChangePasswordClick() },
                        ///modifier= Modifier.width(125.dp)
                    ) {
                        Text(
                            text = "Change Password",
                            fontSize = MaterialTheme.typography.bodyLarge.fontSize,
                        )
                    }
                }
            }
        }
        MenuSection(value = "ACCOUNT")
        Surface(
            tonalElevation = 3.dp
        ) {
            Column {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .drawBehind {
                            drawLine(
                                color = Color.Gray,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = (1).dp.toPx()
                            )
                        }
                        .padding(end = 5.dp),
                    horizontalArrangement = Arrangement.End,
                ) {
                    TextButton(
                        onClick = {
                            onAccountDeleteClick()
                        },
                        //modifier= Modifier.width(60.dp)
                    ) {
                        Text(
                            text = "Delete Account",
                            fontSize = MaterialTheme.typography.bodyLarge.fontSize,
                            color = Color.Red
                        )
                    }
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .drawBehind {
                            drawLine(
                                color = Color.Gray,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = (1).dp.toPx()
                            )
                        }
                        .padding(end = 5.dp),
                    horizontalArrangement = Arrangement.End,
                ) {
                    TextButton(
                        onClick = {
                            onSignOutClick()
                        },
                        //modifier= Modifier.width(60.dp)
                    ) {
                        Text(
                            text = "Sign Out",
                            fontSize = MaterialTheme.typography.bodyLarge.fontSize,
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun MenuSection(
    value: String
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
    ) {
        Text(
            text = value,
            color = MaterialTheme.colorScheme.primary,
            fontSize = MaterialTheme.typography.bodyLarge.fontSize,
        )
    }
}