package com.example.geargenius.common.composable

import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.IconButton
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.model.Creature
import com.example.geargenius.model.Spell
import com.example.geargenius.navigation.DetailsScreen

@Composable
fun ResultBox(
    height: Dp,
    selectedCreature: Creature,
    incBoost: Map<String, Int?>,
    incResist: Map<String, Int?>,
    damageDealt: Int?,
    remainingHealth: Int?,
) {

    val title = if (selectedCreature.school == null) {
        "No creature currently selected."
    } else {
        (selectedCreature.type?.let { "$it: " } ?: "Creature: ") +
                "Rnk. ${selectedCreature.rank}, " +
                "School: ${selectedCreature.school}"
                /*"Inc. Boost ${selectedCreature.inc_boost}, " +
                "Inc. Resist: ${selectedCreature.inc_resist}"*/
    }

    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .drawBehind {
                drawLine(
                    color = Color.Gray,
                    start = Offset(0f, size.height),
                    end = Offset(size.width, size.height),
                    strokeWidth = (1).dp.toPx()
                )
            },
            //.height(220.dp),
        tonalElevation = 3.dp
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 5.dp, bottom = 7.dp, start = 15.dp, end = 10.dp),
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .heightIn(40.dp)
            ) {
                ResultBoxTitle(value = title)
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly,
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CalculationNumbersText(value = if (selectedCreature.health != null) selectedCreature.health.toString() else "0")
                    CalculationLabelsText(value = "Health")
                }
                CalculationNumbersText(value = "-")
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CalculationNumbersText(value = damageDealt?.toString() ?: "0")
                    CalculationLabelsText(value = "Damage")
                }
                CalculationNumbersText(value = "=")
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CalculationNumbersText(value = if (remainingHealth != null && remainingHealth > 0) remainingHealth.toString() else "0")
                    CalculationLabelsText(value = "Remaining")
                }
            }
            if(incBoost.isNotEmpty() || incResist.isNotEmpty()) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 15.dp, bottom = 10.dp, start = 15.dp, end = 10.dp)
                        .animateContentSize(animationSpec = tween(durationMillis = 500))
                        .height(height),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Column(
                        modifier = Modifier.weight(0.5f),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Row(
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(text= "BOOST: ")
                        }
                        incBoost.forEach { (key, value) ->
                            Row(
                                horizontalArrangement = Arrangement.Start
                            ) {
                                Text("$key: $value%")
                            }
                        }
                    }
                    Column(
                        modifier = Modifier.weight(0.5f),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Row(
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(text= "RESIST: ")
                        }
                        incResist.forEach { (key, value) ->
                            Row(
                                horizontalArrangement = Arrangement.Start
                            ) {
                                Text("$key: $value%")
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun CalculationBox(
    openUp: (String) -> Unit,
    appViewModel: GearGeniusAppViewModel,
    title: String,
    selectedSpells: List<Spell>,
    damageDealt: Int?
) {

    Surface(
        modifier = Modifier
            .padding(top = 20.dp, bottom = 20.dp)
            .fillMaxWidth(),
        tonalElevation = 3.dp
    ) {
        Column {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (0.9).dp.toPx()
                        )
                    },
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier
                        .weight(0.7f)
                        .padding(20.dp),
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(
                        text = title,
                        fontWeight = FontWeight.Bold,
                        fontSize = MaterialTheme.typography.bodyLarge.fontSize
                    )
                }
                Column(
                    modifier = Modifier
                        .weight(0.3f)
                        .padding(20.dp),
                    horizontalAlignment = Alignment.End
                ) {
                    Text(
                        text = damageDealt?.toString() ?: "0",
                        fontWeight = FontWeight.Bold,
                        fontSize = MaterialTheme.typography.bodyLarge.fontSize
                    )
                }
            }
            selectedSpells.forEach {spell ->
                Row(
                    modifier= Modifier
                        .fillMaxWidth()
                        .drawBehind {
                            drawLine(
                                color = Color.Gray,
                                start = Offset(0f, size.height),
                                end = Offset(size.width, size.height),
                                strokeWidth = (1).dp.toPx()
                            )
                        }
                        .padding(start = 20.dp, end = 10.dp, top = 15.dp, bottom = 15.dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Column(
                        modifier = Modifier
                            .weight(0.4f),
                        //.padding(start = 20.dp),
                        horizontalAlignment = Alignment.Start
                    ) {
                        Text(
                            text =
                            if (spell.subtype != null)
                                spell.subtype.toString()
                            else
                                spell.type.toString()
                        )
                    }
                    Column(
                        modifier = Modifier
                            .weight(0.6f),
                        //.padding(end = 20.dp),
                        horizontalAlignment = Alignment.Start
                    ) {
                        Text(text = spell.description.toString())
                    }
                }
            }
            Row(
                modifier= Modifier
                    .fillMaxWidth()
                    .drawBehind {
                        drawLine(
                            color = Color.Gray,
                            start = Offset(0f, size.height),
                            end = Offset(size.width, size.height),
                            strokeWidth = (1).dp.toPx()
                        )
                    },
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier
                        .weight(0.5f)
                        .padding(start = 20.dp),
                    horizontalAlignment = Alignment.Start
                ) {
                    IconButton(
                        onClick = {
                            openUp(DetailsScreen.SearchScreen.route)
                        },
                        modifier= Modifier.width(90.dp)
                    ) {
                        Text(
                            text = "ADD SPELL",
                            fontWeight = FontWeight.Bold,
                            color = MaterialTheme.colorScheme.primary
                        )
                    }
                }
                Column(
                    modifier = Modifier
                        .weight(0.5f)
                        .padding(end = 20.dp),
                    horizontalAlignment = Alignment.End
                ) {
                    IconButton(
                        onClick = {
                            appViewModel.onClearAllSpells()
                        },
                        modifier= Modifier.width(90.dp)
                    ) {
                        Text(
                            text = "CLEAR ALL",
                            fontWeight = FontWeight.Bold,
                            //fontSize = MaterialTheme.typography.bodyLarge.fontSize
                            color = Color.Red
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun CalculationNumbersText(
    value: String
) {
    Text(
        text = value,
        style = TextStyle(
            fontWeight = FontWeight.Medium,
            fontSize = 24.sp
        )
    )
}

@Composable
fun CalculationLabelsText(
    value: String
) {
    Text(
        text = value,
        color = LocalContentColor.current.copy(alpha = 0.6f),
        fontSize = 14.sp
    )
}

@Composable
fun ResultBoxTitle(
    value: String
) {
    Text(
        text = value,
        fontWeight = FontWeight.Medium,
        fontSize = 20.sp
    )
}