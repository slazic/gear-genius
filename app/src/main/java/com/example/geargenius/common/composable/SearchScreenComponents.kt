package com.example.geargenius.common.composable

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Sort
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.geargenius.GearGeniusAppViewModel
import com.example.geargenius.model.Creature
import com.example.geargenius.model.Spell
import com.example.geargenius.navigation.DetailsScreen
import com.example.geargenius.screens.search.SearchViewModel
import kotlinx.coroutines.delay

@Composable
fun LoadingIndicator() {
        CircularProgressIndicator(
            modifier = Modifier
                .size(40.dp)
                .padding(8.dp)
                .wrapContentSize(Alignment.Center)
        )
}

@Composable
fun SearchBox(
    modifier: Modifier,
    searchViewModel: SearchViewModel
) {

    val uiState by searchViewModel.uiState

    Surface(
        modifier = modifier,
        tonalElevation = 3.dp
    ) {
        Column {
            SearchAppBar(
                value = uiState.searchQuery,
                onValueChanged = {
                    searchViewModel.onSearchQueryChanged(it)
                },
                onSearchClicked = {
                    searchViewModel.onSearchClicked(it)
                }
            )
            Box(
                modifier
                    .fillMaxWidth()
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly
                ) {
                    Button(
                        modifier = Modifier
                            .weight(0.5f)
                            .fillMaxHeight()
                            .drawBehind {
                                if (uiState.creaturesSelected) {
                                    drawLine(
                                        color = Color.Cyan,
                                        start = Offset(0f, size.height),
                                        end = Offset(size.width, size.height),
                                        strokeWidth = 10.dp.toPx()
                                    )
                                }
                            },
                        onClick = {
                            searchViewModel.onCreaturesSelected()
                        },
                        shape = RoundedCornerShape(0.dp),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color.Transparent,
                            contentColor = LocalContentColor.current.copy(alpha = if(uiState.creaturesSelected) 1f else 0.5f )
                        )
                    ) {
                        Text(
                            text= "Creatures",
                            fontSize = MaterialTheme.typography.bodyMedium.fontSize
                        )
                    }
                    Button(
                        modifier = Modifier
                            .weight(0.5f)
                            .fillMaxHeight()
                            .drawBehind {
                                if (uiState.spellsSelected) {
                                    drawLine(
                                        color = Color.Cyan,
                                        start = Offset(0f, size.height),
                                        end = Offset(size.width, size.height),
                                        strokeWidth = 10.dp.toPx()
                                    )
                                }
                            },
                        onClick = {
                            searchViewModel.onSpellsSelected()
                        },
                        shape = RoundedCornerShape(0.dp),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color.Transparent,
                            contentColor = LocalContentColor.current.copy(alpha = if(uiState.spellsSelected) 1f else 0.5f )
                        )
                    ) {
                        Text("Spells")
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchAppBar(
    value: String,
    onValueChanged: (String) -> Unit,
    onSearchClicked: (String) -> Unit,
) {
    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 5.dp, start = 20.dp, end = 20.dp),

        value = value,
        onValueChange = {
            onValueChanged(it)
        },
        leadingIcon = {
            Icon(
                modifier = Modifier.alpha(alpha = 0.7f),
                imageVector = Icons.Default.Search,
                contentDescription = "search"
            )
        },
        trailingIcon = {
            if(value.isNotEmpty()){
                IconButton(onClick = {
                    onValueChanged("")
                }) {
                    Icon(
                        imageVector = Icons.Default.Close,
                        contentDescription = "close"
                    )
                }
            }
        },
        placeholder = {
            Text(
                modifier = Modifier.alpha(alpha=0.7f),
                text = "Search...",
            )
        },
        singleLine = true,
        maxLines = 1,

        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Search
        ),
        keyboardActions = KeyboardActions(
            onSearch = {
                onSearchClicked(value)
            }
        ),
        colors = TextFieldDefaults.textFieldColors(
            containerColor = MaterialTheme.colorScheme.surface,
        ),
        shape = RoundedCornerShape(50.dp)
    )
}

@Composable
fun ResultCard(
    openUp: (String) -> Unit,
    item: Any?,
    appViewModel: GearGeniusAppViewModel
) {

    var isIconDone by remember { mutableStateOf(false) }

    LaunchedEffect(isIconDone) {
        if (isIconDone) {
            delay(2000)
            isIconDone = false
        }
    }

    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier.padding(10.dp,5.dp,10.dp,10.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation =  10.dp,
        ),
        colors = CardDefaults.cardColors(
        ),
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(70.dp)
                .clickable(onClick = {
                    openUp(DetailsScreen.InformationScreen.route)
                }),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                modifier = Modifier
                    .weight(0.3f)
                    .padding(5.dp)
                    .border(
                        width = 1.dp,
                        color = MaterialTheme.colorScheme.surface.copy(alpha = 0.38f),
                        shape = RoundedCornerShape(8.dp)
                    ),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    modifier = Modifier.padding(top=3.dp),
                    text =
                        when (item) {
                            is Creature -> "Rnk."
                            is Spell -> "Pips req."
                            else -> ""
                        },
                    style = TextStyle(
                        fontSize = 13.sp,
                    ),
                    color = LocalContentColor.current.copy(alpha = 0.6f)
                )
                Text(
                    text =
                        when (item) {
                            is Creature -> item.rank.toString()
                            is Spell -> item.pip_cost.toString()
                            else -> ""
                        },
                    style = TextStyle(
                        fontSize = 34.sp,
                        fontWeight = FontWeight.Bold,
                    ),
                )
            }
            Column(
                modifier = Modifier
                    .weight(0.6f)
                    .padding(start = 15.dp),
                horizontalAlignment = Alignment.Start
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    when (item) {
                        is Creature -> item.type
                        is Spell -> item.type
                        else -> ""
                    }?.let {
                        Text(
                            modifier= Modifier.padding(end=5.dp),
                            text =
                            it,
                            style = TextStyle(
                                fontSize = 30.sp,
                                fontWeight = FontWeight.Bold,
                            ),
                        )
                    }
                    when (item) {
                        is Creature -> item.classification
                        is Spell -> item.subtype
                        else -> ""
                    }?.let {
                        Text(
                            text =
                            it,
                        )
                    }
                }
                when (item) {
                    is Creature -> item.school
                    is Spell -> item.school
                    else -> ""
                }?.let {
                    Text(
                        text =
                        it,
                    )
                }
            }
            Column(
                modifier = Modifier
                    .weight(0.2f)
                    .padding(end = 10.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                IconButton(
                    onClick = {
                        when (item) {
                            is Creature -> appViewModel.onCreatureAdd(item)
                            is Spell -> {
                                appViewModel.onSpellAdd(item)
                            }
                            else -> {}
                        }

                        isIconDone = true
                    },
                    modifier = Modifier
                        .height(50.dp)
                        .width(50.dp)
                        .clip(shape = CircleShape),
                    colors = IconButtonDefaults.iconButtonColors(
                        containerColor = MaterialTheme.colorScheme.surface,
                    ),
                    enabled = !isIconDone
                ) {
                    if ( isIconDone ) {
                        Icon(
                            imageVector = Icons.Default.Done,
                            contentDescription = "",
                            tint = Color.Green,
                        )
                    } else {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = "",
                            tint = MaterialTheme.colorScheme.primary
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun ResultMenu(
    initialText: String,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            modifier = Modifier
                .padding(20.dp)
                .weight(0.6f),
            text = initialText,
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                fontStyle = FontStyle.Normal
            ),
        )
        IconButton(
            modifier = Modifier
                .padding(end = 20.dp)
                .fillMaxWidth()
                .weight(0.4f),
            onClick = { /*TODO*/ },
            /*colors = ButtonDefaults.buttonColors(
                containerColor = Color.Transparent
            )*/
        ) {
            Row {
                Icon(
                    imageVector = Icons.Default.Sort,
                    contentDescription = "sort",
                    tint = LocalContentColor.current.copy()
                )
                Text(
                    text = "Sort by"
                )
            }
        }
    }
}

@Composable
fun ListContent(
    state: LazyListState,
    openUp: (String) -> Unit,
    cardInformation: List<*>,
    appViewModel: GearGeniusAppViewModel
) {
    LazyColumn(
        state = state
    ) {
        items(
            items = cardInformation
        ) { item ->
            ResultCard(
                item= item,
                appViewModel= appViewModel,
                openUp= openUp,
            )
        }
    }
}