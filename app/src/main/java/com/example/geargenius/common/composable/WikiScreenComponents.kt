package com.example.geargenius.common.composable

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.example.geargenius.R
import com.example.geargenius.navigation.DetailsScreen

sealed class WikiHomeCardInformation(
    val image: Int,
    val title: String,
    val description: String
) {

    object Loremaster: WikiHomeCardInformation(
        image = R.drawable.loremaster,
        title = "Loremaster",
        description = "Rank: 9, Boss. School: Balance"
    )

    object Jabberwock: WikiHomeCardInformation(
        image = R.drawable.jabberwock,
        title = "Jabberwock",
        description = "Rank: 11, Boss. School: Fire"
    )

    object Zeus: WikiHomeCardInformation(
        image = R.drawable.zeus,
        title = "Zeus Sky Father",
        description = "Rank: 7, Boss. School: Myth"
    )

    object LordNightshade: WikiHomeCardInformation(
        image = R.drawable.lordnightshade,
        title = "Lord Nightshade",
        description = "Rank: 3, Boss. School: Death"
    )

    object TheHarvestLord: WikiHomeCardInformation(
        image = R.drawable.harvestlord,
        title = "The Harvest Lord",
        description = "Rank: 3, Boss. School: Fire"
    )
}


@Composable
fun WikiCard(
    information: WikiHomeCardInformation,
    openUp: (String) -> Unit
) {
    Card(
        shape = RoundedCornerShape(8.dp),
        modifier = Modifier.padding(10.dp,5.dp,10.dp,10.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation =  10.dp,
        ),
        colors = CardDefaults.cardColors(
        ),
    ) {
        Column(
            modifier = Modifier
                .clickable(onClick = {
                    openUp(DetailsScreen.InformationScreen.route)
                })
        ) {

            Image(
                painter = painterResource(information.image),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .height(150.dp)
                    .fillMaxWidth()
            )

            Column(modifier = Modifier.padding(16.dp)) {
                Text(
                    text = information.title,
                    style = MaterialTheme.typography.titleMedium,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )

                Spacer(modifier = Modifier.height(5.dp))

                Text(
                    text = information.description,
                    //maxLines = 1,
                    //overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.titleSmall,
                )
            }
        }
    }
}