package com.example.geargenius.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SearchResult<T>(
    @SerialName("Results")
    val results: List<T>
)
