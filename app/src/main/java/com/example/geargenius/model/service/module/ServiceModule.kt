package com.example.geargenius.model.service.module

import com.example.geargenius.model.service.AccountService
import com.example.geargenius.model.service.DataService
import com.example.geargenius.model.service.implementation.AccountServiceImpl
import com.example.geargenius.model.service.implementation.DataServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class ServiceModule {
    @Binds
    abstract fun provideAccountService(impl: AccountServiceImpl): AccountService

    @Binds
    abstract fun provideDataService(impl: DataServiceImpl): DataService

    //@Binds
    //abstract fun provideLogService(impl: LogServiceImpl): LogService

    //@Binds
    //abstract fun provideStorageService(impl: StorageServiceImpl): StorageService

    //@Binds
    //abstract fun provideConfigurationService(impl: ConfigurationServiceImpl): ConfigurationService
}