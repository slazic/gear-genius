package com.example.geargenius.model.service.implementation

import android.util.Log
import com.example.geargenius.model.service.AccountService
import com.example.geargenius.utility.Resource
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class AccountServiceImpl @Inject constructor(
    private val auth: FirebaseAuth
) : AccountService {

    override val currentUser: FirebaseUser?
        get() = auth.currentUser

    override val isUserAuthenticated: Boolean
        get() = auth.currentUser != null

    override suspend fun signIn(email: String, password: String): Resource<AuthResult> {
        return try {
            val result = auth.signInWithEmailAndPassword(email, password).await()
            Resource.Success(result)
        } catch (e: Exception) {
            Resource.Exception(e)
        }
    }

    override suspend fun sendEmailVerification(): Resource<Boolean> {
        return try {
            auth.currentUser?.sendEmailVerification()?.await()
            Resource.Success(true)
        } catch (e: Exception) {
            Resource.Exception(e)
        }
    }

    override suspend fun sendPasswordResetEmail(email: String): Resource<Boolean> {
        return try {
            auth.sendPasswordResetEmail(email).await()
            Resource.Success(true)
        } catch (e: Exception) {
            Resource.Exception(e)
        }
    }

    override suspend fun signUp(email: String, password: String): Resource<AuthResult> {
        return try {
            val result = auth.createUserWithEmailAndPassword(email, password).await()
            Resource.Success(result)
        } catch (e: Exception) {
            Resource.Exception(e)
        }
    }

    override suspend fun changeEmail(email: String, password: String, newEmail: String) {
        try {
            val credential = EmailAuthProvider.getCredential(email, password)

            auth.currentUser!!.reauthenticate(credential).await()

            auth.currentUser!!.updateEmail(newEmail).await()
        } catch (e: Exception) {
            Log.d("exception", e.toString())
        }
    }

    override suspend fun changePassword(
        email: String,
        currentPassword: String,
        newPassword: String
    ) {
        try {
            val credential = EmailAuthProvider.getCredential(email, currentPassword)

            auth.currentUser!!.reauthenticate(credential).await()

            auth.currentUser!!.updatePassword(newPassword).await()
        } catch (e: Exception) {
            Log.d("exception", e.toString())
        }
    }

    override suspend fun deleteAccount(email: String, password: String) {
        try {
            val credential = EmailAuthProvider.getCredential(email, password)

            auth.currentUser!!.reauthenticate(credential).await()

            auth.currentUser!!.delete().await()
        } catch (e: Exception) {
            Log.d("exception", e.toString())
        }
    }

    override fun signOut(): Resource<Boolean> {
        return try {
            auth.signOut()
            Resource.Success(true)
        } catch (e: Exception) {
            Resource.Exception(e)
        }
    }

    override fun getAuthState(viewModelScope: CoroutineScope) = callbackFlow {
        val authStateListener = FirebaseAuth.AuthStateListener { auth ->
            trySend(auth.currentUser == null)
        }
        auth.addAuthStateListener(authStateListener)
        awaitClose {
            auth.removeAuthStateListener(authStateListener)
        }
    }.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), auth.currentUser == null)

}