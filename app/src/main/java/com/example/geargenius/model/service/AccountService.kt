package com.example.geargenius.model.service

import com.example.geargenius.utility.Resource
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.StateFlow

interface AccountService {
    //val currentUserId: String
    val isUserAuthenticated: Boolean
    val currentUser: FirebaseUser?

    suspend fun signIn(email: String, password: String): Resource<AuthResult>
    suspend fun sendEmailVerification(): Resource<Boolean>
    suspend fun sendPasswordResetEmail(email: String): Resource<Boolean>
    suspend fun signUp(email: String, password: String): Resource<AuthResult>

    suspend fun changeEmail(email: String, password: String, newEmail: String)

    suspend fun changePassword(email:String, currentPassword: String, newPassword: String)

    suspend fun deleteAccount(email: String, password: String)

    fun signOut(): Resource<Boolean>
    fun getAuthState(viewModelScope: CoroutineScope): StateFlow<Boolean>
}