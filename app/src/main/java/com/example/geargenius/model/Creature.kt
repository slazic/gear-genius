package com.example.geargenius.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Creature(
    @SerialName("Type")
    val type: String? = null,
    @SerialName("Classification")
    val classification: String? = null,
    @SerialName("Rank")
    val rank: Int? = null,
    @SerialName("Health")
    val health: Int? = null,
    @SerialName("School")
    val school: String? = null,
    @SerialName("Cheats")
    val cheats: Boolean? = null,
    @SerialName("IncBoost")
    val inc_boost: String? = null,
    @SerialName("IncResist")
    val inc_resist: String? = null,
    @SerialName("World")
    val world: String? = null,
    @SerialName("Location")
    val location: String? = null,
    @SerialName("Sublocation")
    val sub_location: String? = null,
)
