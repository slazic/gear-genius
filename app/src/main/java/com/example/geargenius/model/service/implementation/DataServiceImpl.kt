package com.example.geargenius.model.service.implementation

import android.util.Log
import com.example.geargenius.data.remote.Wizard101WikiApi
import com.example.geargenius.model.Creature
import com.example.geargenius.model.SearchResult
import com.example.geargenius.model.Spell
import com.example.geargenius.model.service.DataService
import com.example.geargenius.utility.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DataServiceImpl @Inject constructor(
    private val api: Wizard101WikiApi
) : DataService {

    override suspend fun searchCreature(creatureName: String): Flow<Resource<SearchResult<Creature>>> {
        return flow {
            try {
                val result = Resource.Success(api.searchCreature(creatureName= creatureName))
                Log.d("uiState", result.data.toString())
                Log.d("uiState", result.message.toString())
                Log.d("uiState", result.exception.toString())
                emit(result)
            } catch (e: Exception) {
                val result = Resource.Exception<SearchResult<Creature>>(e)
                Log.d("uiState", result.data.toString())
                Log.d("uiState", result.message.toString())
                Log.d("uiState", result.exception.toString())
                emit(result)
            }
        }
    }

    override suspend fun searchSpell(spellName: String): Flow<Resource<SearchResult<Spell>>> {
        return flow {
            try {
                val result = Resource.Success(api.searchSpell(spellName= spellName))
                Log.d("uiState", result.data.toString())
                Log.d("uiState", result.message.toString())
                Log.d("uiState", result.exception.toString())
                emit(result)
            } catch (e: Exception) {
                val result = Resource.Exception<SearchResult<Spell>>(e)
                Log.d("uiState", result.data.toString())
                Log.d("uiState", result.message.toString())
                Log.d("uiState", result.exception.toString())
                emit(result)
            }
        }
    }
}

