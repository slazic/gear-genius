package com.example.geargenius.model.service

import com.example.geargenius.model.Creature
import com.example.geargenius.model.SearchResult
import com.example.geargenius.model.Spell
import com.example.geargenius.utility.Resource
import kotlinx.coroutines.flow.Flow

interface DataService {

    suspend fun searchCreature(creatureName: String) : Flow<Resource<SearchResult<Creature>>>

    suspend fun searchSpell(spellName: String) : Flow<Resource<SearchResult<Spell>>>

}