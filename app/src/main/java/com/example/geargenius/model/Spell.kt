package com.example.geargenius.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Spell(
    @SerialName("PipCost")
    val pip_cost: Int? = null,
    @SerialName("School")
    val school: String? = null,
    @SerialName("Accuracy")
    val accuracy: String? = null,
    @SerialName("Description")
    val description: String? = null,
    @SerialName("Type")
    val type: String? = null,
    @SerialName("Subtype")
    val subtype: String? = null,
)
