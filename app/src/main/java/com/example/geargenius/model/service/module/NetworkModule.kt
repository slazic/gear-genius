package com.example.geargenius.model.service.module

import com.example.geargenius.data.remote.Wizard101WikiApi
import com.example.geargenius.utility.Constants.API_BASE_URL
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            //.readTimeout(15, TimeUnit.MILLISECONDS)
            //.connectTimeout(15, TimeUnit.MILLISECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {

        val contentType = MediaType.get("application/json")
        val json = Json {
            ignoreUnknownKeys = true //not using all fields in response
        }

        return Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(json.asConverterFactory(contentType))
            .build()
    }

    @Provides
    @Singleton
    fun provideWizard101WikiApi(retrofit: Retrofit): Wizard101WikiApi {
        return retrofit.create(Wizard101WikiApi::class.java)
    }

}